

public class Aritmetica{

   public static int division(int numerador, int denominador) 
      throws OperacionExcepcion {
      
      if(denominador == 0){
        // throw me permite lanzar una excepcion propia
        throw new OperacionExcepcion("Division por cero");
      }
      
      return numerador/denominador;
   
   }
   
   public static double division(double numerador, double denominador) 
      throws OperacionExcepcion {
      
      if(denominador == 0){
        // throw me permite lanzar una excepcion propia
        throw new OperacionExcepcion("Division por cero");
      }
      
      return numerador/denominador;
   
   }
   
   
  //   Character.isDigit('a');
     
     
     
}